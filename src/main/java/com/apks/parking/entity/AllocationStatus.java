package com.apks.parking.entity;

public enum AllocationStatus {

	ALLOCATED, PENDING ,REJECTED
}
