package com.apks.parking.entity;

import java.time.LocalDate;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Table(name = "allotments")
public class Allotments {

	@Id
	private Long allotmentId;
	@OneToOne
	private Employees employees;
	private String parkingId;
	private LocalDate date;
	@Enumerated(EnumType.STRING)
	private AllocationStatus allocationStatus;
	
}
