package com.apks.parking.entity;

public enum ParkingStatus {

	AVAILABLE, NOT_AVAILABLE
}
