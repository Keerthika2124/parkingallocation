package com.apks.parking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ParkingAllocationApplication {

	public static void main(String[] args) {
		SpringApplication.run(ParkingAllocationApplication.class, args);
	}

}
